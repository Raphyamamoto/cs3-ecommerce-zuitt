import './App.css';
import React, {useState, useEffect, useContext} from 'react';
import Navbar from './components/AppNavBar';
import Footer from './components/AppFooter';
import Bahay from './pages/Home';
import GetProducts from './pages/Products';
import SingleProduct from './pages/SingleProduct';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Contact from './pages/Contact';
import Register from './pages/Register';
import Error from './pages/Error';
import AdminPage from './pages/Admin';
import AddProductAdmin from './pages/CreateProduct';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import { UserProvider } from './UserContext';
import Search from './pages/Search';

const App = () => {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  
    setUser({
        id: null,
        isAdmin: null
    })
  };

  useEffect(() => {
    const authorization = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('access')}`
      }
    }
    fetch('https://evening-earth-18907.herokuapp.com/user/profile', authorization)
      .then(detailsPromise => {
        console.log(detailsPromise)
        if (detailsPromise.ok) {
          return detailsPromise.json()
        } else {
          throw new Error('Error Occured')
        }
      })
      .then(details => {
        console.log(details)
        if (details._id !== null || details._id !== undefined || details._id !== 'undefined' || details._id !== false) {
          setUser({
            id: details._id,
            isAdmin: details.isAdmin,
          })
          console.log("This went if branch")
        } else {
          setUser({
            id: null,
            isAdmin: null,
          })
          console.log("This went else branch")
        }
      }).catch(error => console.error(error))
      console.log(user)
  }, [])
  

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/features" component={Bahay} />
          <Route exact path="/home" component={Bahay} />
          <Route exact path="/products" component={GetProducts} />
          <Route exact path="/item/:id" component={SingleProduct} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/logout" component= {Logout}/>
          <Route exact path="/create" component= {AddProductAdmin}/>
          <Route exact path="/register" component={Register} />
          <Route exact path="/contacts" component={Contact} />
          <Route exact path="/admin" component={AdminPage} />
          <Route exact path="/search" component={Search} />
          <Route component={Error} />
        </Switch>
      <Footer />
      </Router>
    </UserProvider>
  )
}

export default App;


