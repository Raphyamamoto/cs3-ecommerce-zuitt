import React from 'react';

const UserContext = React.createContext() //a context object will be CREATED

export const UserProvider = UserContext.Provider

export default UserContext;