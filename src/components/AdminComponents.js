import {Row, Col,} from 'react-bootstrap';
import React from 'react';


export default function AdminBanner({head}) {
    const {title} = head
    return (
        <Row className="text-center">
            <Col>
            <div>
                <h1 id="banner1">{title}</h1>
            </div>
            </Col>
        </Row>
    )
}

  