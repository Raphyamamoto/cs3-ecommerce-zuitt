import {Row, Col} from 'react-bootstrap';
import React from 'react';
import { AiFillFacebook, AiFillGithub, AiFillInstagram, AiFillLinkedin } from "react-icons/ai";



const AppFooter = () => {
    return(
        <div className="p-3 mt-5 border border-top text-light bg-dark" id="footer1">
            <Row className="wrapper">
                <Col className="d-flex justify-content-start pl-5"> Any questions? Let us help you <br/> Call us: +818066944625 </Col>
                <Col className="d-flex justify-content-center"> 

                    <div className="mr-4 icon instagram">
                        <div className="tooltip">Instagram</div>
                            <a id="span" href="https://www.instagram.com/">
                                <AiFillInstagram size="1.3em" /> 
                            </a>
                    </div>

                    <div className="mr-4 icon facebook">
                        <div className="tooltip">Facebook</div>
                            <a id="span" href="https://www.facebook.com/">
                                <AiFillFacebook size="1.3em" /> 
                            </a>
                    </div>

                    <div className="mr-4 icon linkedin">
                        <div className="tooltip">Linkedin</div>
                        <a id="span" href="https://www.linkedin.com/">
                            <AiFillLinkedin size="1.3em" />  
                        </a>
                    </div>

                    <div className="mr-4 icon github">
                        <div className="tooltip">Github</div>
                        <a id="span" href="https://raphaelyamamoto.github.io/WebPortfolio-Yammy/">
                            <AiFillGithub size="1.3em" /> 
                        </a>
                    </div>

                </Col>
                <Col className="d-flex justify-content-end pr-5 mt-2">  Copyright &copy; 2021 Denwa.All rights reserved </Col>
            </Row>
        </div>
    )
}
// fixed-bottom sticky-top/bottom
export default AppFooter;