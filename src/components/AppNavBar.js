import {Nav, Navbar, NavDropdown, Container, Form, Button} from 'react-bootstrap';
import React, { useState, useEffect, Fragment, useContext } from 'react';
import { NavLink, useHistory, Route } from 'react-router-dom';
import { IoCartOutline } from "react-icons/io5";
import { AiOutlineUser } from "react-icons/ai";
import UserContext from '../UserContext';
import ImageCard from './ImageCard';
import Search from '../pages/Search';




const AppNavBar = () => {
    const {user, setUser} = useContext(UserContext);
    const history = useHistory();
    const [products, setProducts] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        fetch('https://evening-earth-18907.herokuapp.com/product/all').then(res => res.json()).then(convertedData => {
           setProducts(convertedData);
           console.log(convertedData);
        })
    },[])

    let filterProducts = products.filter(items => {
        return(Object.values(items).join(" ").toLowerCase().includes(search.toLowerCase()))
    });

    const searchProducts = () => {filterProducts.map((item) => {
        console.log(item)
        return(
            <ImageCard key={item._id} productsInfo={item}/>
        )
    })
}

    function logOut() 
    {
        localStorage.clear()
        history.push('/login')
    }

    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        console.log(user)

    })
    useEffect(() => {
        console.log(user.id)
        if (user.id === null || user.id === undefined || user.id === "undefined") {
            setIsLoggedIn(false)
        } else {
            setIsLoggedIn(true)
        }
    },[user])
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand as={NavLink} to="/">
                <img src="/pics/denwa2.png" width="70" height="70" className="d-inline-block align-top justify-content-start" alt="not found"/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-center">
                        <Nav className="me-auto" id="navOne">
                            {/* <Nav.Link id="featureAnchor"><a href="#">Features</a></Nav.Link> */}
                            <Nav.Link id="featureAnchor" as={NavLink} to="/home">Features</Nav.Link>
                            <Nav.Link id="productAnchor" href="#">Products</Nav.Link>
                            <NavDropdown title="Categories" id="collasible-nav-dropdown" menuVariant="dark">
                                <NavDropdown.Item as={NavLink} to="/products">Mobile Phones</NavDropdown.Item>
                            </NavDropdown>
                            <Nav>
                                <Nav.Link as={NavLink} to="/contacts">Contacts</Nav.Link>
                            </Nav>
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                        <Nav className="me-auto">
                            <NavDropdown title={<AiOutlineUser size="1.3em"/>} id="collasible-nav-dropdown"> 
                                {(!isLoggedIn) ? 
                                <Fragment>
                                    <NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item>
                                    <NavDropdown.Item as={NavLink} to="/register">Create an account</NavDropdown.Item>
                                </Fragment>
                                :
                                    <NavDropdown.Item as={NavLink} to="/logout" >Logout</NavDropdown.Item>
                                }
                            </NavDropdown>
                        <Nav.Link id="cartIcon"> 
                            <IoCartOutline size="1.3em" /> 
                        </Nav.Link>       
                        </Nav>
                        <Form className="d-flex">
                            <Form.Control type="search" placeholder="Search" className="me-2 ml-3" aria-label="Search" onClick={event => setSearch(event.target.value)} />
                            <Button as={NavLink} to="/search" variant="outline-light">Search</Button>
                        </Form>
                        {/* <Route render={({ history }) => <Search history={history} /> } /> */}
                    </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default AppNavBar;