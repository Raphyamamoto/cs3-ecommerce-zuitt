import {Row, Col,} from 'react-bootstrap';
import React from 'react';


export default function Banner({props}) {
    const {title, paragraph} = props
    return (
        <Row className="text-center">
            <Col>
            <div>
                <h1 id="banner1">{title}</h1>
                <h4 id="banner2">{paragraph}</h4>
            </div>
            </Col>
        </Row>
    )
}

  