// import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import Card from '@material-ui/core/Card';

// import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
// import Typography from '@material-ui/core/Typography';
// import { Collapse } from '@material-ui/core';

// const useStyles = makeStyles({
//   root: {
//     maxWidth: 700,
//     background: 'rgba(0,0,0,0.5)',
//     margin: '20px',
//   },
//   media: {
//     height: 300,
//     width: 500
    
//   },
//   title: {
//     fontFamily: 'Poppins',
//     fontWeight: 'bold',
//     fontSize: '2rem',
//     color: '#fff',
//   },
//   desc: {
//     fontFamily: 'Poppins',
//     fontSize: '1.1rem',
//     color: '#ddd',
//   },
// });

// export default function ImageCard({ items , checked }) {
//   const classes = useStyles();

//   return (
//     // <Collapse in={checked} {...(checked ? { timeout: 1000 } : {})}>
//       <Card className={classes.root}>
//         <CardMedia
//           className={classes.media}
//           image={items.imageUrl}
//         />
//         <CardContent>
//           <Typography
//             gutterBottom
//             variant="h5"
//             component="h1"
//             className={classes.title}
//           >
//             {items.title}
//           </Typography>
//           <Typography
//             variant="body2"
//             color="textSecondary"
//             component="p"
//             className={classes.desc}
//           >
//             {items.description}
//           </Typography>
//         </CardContent>
//       </Card>
//     // </Collapse>
//   );
// }
import Card from 'react-bootstrap/Card';
import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Button, Row, Col } from 'react-bootstrap';

const ImageCard = ({productsInfo}) => {
  const {_id, productName, link, categories, description, price} = productsInfo;
  return(
    <Container id="productCatalog">
      <Row>
        <Col>
          <Card id="productCard">
              <Card.Body>
                <div>
                  <img id="imageCustom" src={link} alt="not found" />
                </div>
                <Card.Title> {productName}</Card.Title>
                  <Card.Subtitle> Categories: </Card.Subtitle>
                  <Card.Text>
                      {categories}
                  </Card.Text>
                  <Card.Subtitle> Product Description: </Card.Subtitle>
                  <Card.Text>
                      {description}
                  </Card.Text>
                  <Card.Subtitle> Product Price: </Card.Subtitle>
                  <Card.Text> PHP {price} </Card.Text>
              </Card.Body>
              <Button variant="outline-dark" id="createAcctButton" className='btn btn-block' as={Link} to={`/item/${_id}`}> View</Button>
          </Card>
        </Col>
        </Row>
    </Container>
  )
}

export default ImageCard;