import React, {Fragment, useState, useEffect} from 'react';
import AdminBanner from '../components/AdminComponents';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


let adminTitle = {
    title: "Welcome Admin!"
};

export default function AdminPage () {
    const [admin, setAdmin] = useState([])
    const [products, setProducts] = useState([])
    const [categories, setCategories] = useState("");
    const [description, setDescription] = useState("");
    const [productId, setProductId] = useState("");
    const [price, setPrice] = useState("");
    const [productName, setProductName] = useState("");
    let token = localStorage.getItem('access');
    
    useEffect(() => {
        retrieveProducts()
    }, [])

      function retrieveProducts() {
        fetch("https://evening-earth-18907.herokuapp.com/product/admin/productlist").then((result) => {
            
          result.json().then((resp) => {
            // console.warn(resp)
            setProducts(resp)
            setProductId(resp[0]._id)
            setProductName(resp[0].productName)
            setCategories(resp[0].categories)
            setDescription(resp[0].description)
            setPrice(resp[0].price)
            console.log(products)
          })
        })
      }
    
      function deleteProducts(itemId) {
        fetch(`https://evening-earth-18907.herokuapp.com/product/archive`, {
          method: 'PUT',
          headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization': `Bearer ${token}`
        },
        body:JSON.stringify({
          "id": itemId
        })

        }).then((result) => {
          result.json().then((resp) => {
            // console.warn(resp)
            retrieveProducts()
          })
        })
      }

      function activateProducts(itemId) {
        fetch(`https://evening-earth-18907.herokuapp.com/product/activate`, {
          method: 'PUT',
          headers:{
            'Accept':'application/json',
            'Content-Type':'application/json',
            'Authorization': `Bearer ${token}`
        },
        body:JSON.stringify({
          "id": itemId
        })

        }).then((result) => {
          result.json().then((resp) => {
            // console.warn(resp)
            retrieveProducts()
          })
        })
      }

      function selectProducts(itemId)
      {
        let item = products.find(info => info._id === itemId)
            setProductId(item._id)
            setProductName(item.productName)
            setDescription(item.description);
            setPrice(item.price)
      }

      function updateProducts() {
        fetch(`https://evening-earth-18907.herokuapp.com/product/update`, {
          method: 'PUT',
          headers:{
              'Accept':'application/json',
              'Content-Type':'application/json',
              'Authorization': `Bearer ${token}`
          },
          body:JSON.stringify({
            "id": productId,
            "productName": productName,
            "description": description,
            "price": price
          })
        }).then((result) => {
          result.json().then((resp) => {
            // console.warn(resp)
            console.log(resp)
            console.log(token)
            retrieveProducts()
          })
        })
      }
    useEffect(() => {
        fetch(`https://evening-earth-18907.herokuapp.com/product/admin/productlist`).then((result) => 
            result.json().then((response) => {
                console.warn(response)
                setAdmin(response)
            })
        )
    },[])
    console.warn(admin)
    return (
        <Fragment>
            <AdminBanner head={adminTitle} />
            <div>
                <table border="1" style={{ float: 'left' }}>
                    <tbody>
                        <tr>
                            <td>Product ID</td>
                            <td>Product Name</td>
                            <td>Description</td>
                            <td>Categories</td>
                            <td>Price</td>
                            <td>Status</td>
                        </tr>
                    {
                        products.map((item, i) =>
                            <tr key={i}>
                                <td>{item._id}</td>
                                <td>{item.productName}</td>
                                <td>{item.description}</td>
                                <td>{item.categories}</td>
                                <td>{item.price}</td>
                                <td>{item.isAvailable.toString()}</td>
                                <td><button onClick={() => deleteProducts(item._id)}>Delete</button></td>
                                <td><button onClick={() => activateProducts(item._id)}>Activate</button></td>
                                <td><button onClick={() => selectProducts(item._id)}>Select</button></td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
                <div>
                    <div>
                        <h1>{productName}</h1>
                    </div>
                    <input type="text" value={productName} onChange={(e)=>{setProductName(e.target.value)}} /> <br /><br />
                    <input type={Number} value={price} onChange={(e)=>{setPrice(e.target.value)}} /> <br /><br />
                    <input type="text" value={description}  onChange={(e)=>{setDescription(e.target.value)}} /> <br /><br />
                    <button onClick={updateProducts} >Update Product</button>  
                </div>
                <div>
                    <Button as={Link} to="/create" variant="outline-dark" id="createAcctButton" className='btn btn-block'> ADD PRODUCTS </Button>
                </div>
            </div>
        </Fragment>
    )
}