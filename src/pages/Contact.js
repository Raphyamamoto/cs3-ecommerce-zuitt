import React from 'react';
import { Row, Col, Form, Button, Container } from 'react-bootstrap';

// const resetDisplay = 
export default function Contacts() {
    return (
        <Container id="contactMain">
            <Row>
                <Col id="getInTouch">
                    <h2>Get in Touch</h2>
                    <p>
                    Please fill out the form below to send us an email and we will
                    get back to you as soon as possible.
                    </p>
                        {/* name */}
                        <Form>
                            <Row>
                                <Col>
                                <Form.Group controlId="name">
                                    <Form.Label></Form.Label>
                                    <Form.Control type="text" placeholder="Name" required />
                                </Form.Group>
                                </Col>
                        {/* Email */}
                                <Col>
                                <Form.Group controlId="email">
                                    <Form.Label></Form.Label>
                                    <Form.Control type="email" placeholder="Email" required />
                                </Form.Group>
                                </Col>
                            </Row>
                            <div>
                                <Form.Group controlId="message">
                                    <Form.Label></Form.Label>
                                    <Form.Control as="textarea" rows={5} placeholder="Message" required />
                                </Form.Group>
                            </div>
                            <div id="buttonsContact">   
                                <Button  variant="outline-primary" type="submit" id="submitBtn" className='btn btn-custom btn-md mt-2'>
                                    SEND MESSAGE
                                </Button>
                                <Button variant="outline-primary" type="reset" id="resetBtn" className='btn btn-custom btn-md mt-2'>
                                    RESET
                                </Button>
                            </div>
                        </Form>
                </Col>
                <Col id="contactInfo">
                    <div className="contact-item" >
                        <h3>Contact Info</h3>
                        <p>
                            <i className='fa fa-map-marker'></i> Address:  <br/> Kanagawa, Japan 
                        </p>
                    </div>
                    <div className="mt-5">
                        <p>
                            <i className='fa fa-map-marker'></i> Phone:  <br/> +8180 6694 4625
                        </p>
                    </div>
                    <div className="mt-5">
                        <p>
                            <i className='fa fa-map-marker'></i> Email:  <br/> acecaparas47@gmail.com 
                        </p>
                    </div>

                </Col>
            </Row>
        </Container>
    )
}