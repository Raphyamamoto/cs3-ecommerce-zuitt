import React, {Fragment, useState, useEffect, useContext} from 'react';
import AdminBanner from '../components/AdminComponents';
import Swal from "sweetalert2";
import { Link, useHistory } from "react-router-dom";
import UserContext from '../UserContext';
import { Form, Button } from 'react-bootstrap';


let create = {
    title: "Add Products!"
}

export default function AddProductAdmin() {
    const history = useHistory()
    const {user} = useContext(UserContext);
    const [ categories, setCategories] = useState('')
    const [ productName, setProductName] = useState('')
    const [ imageLink, setImageLink] = useState('')
    const [ description, setDescription] = useState('')
    const [ price, setPrice] = useState('')

    let token = localStorage.getItem('access')

    function AddProduct(event){
        event.preventDefault();
        
        fetch('https://evening-earth-18907.herokuapp.com/product/add', {
            method: "POST", 
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : `Bearer ${token}`
            },
            body: JSON.stringify({
                "name": productName,
                "link": imageLink,
                "description": description,
                "categories": categories,
                "price": price
            })
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                
                if (data) {
                    Swal.fire({
                    title:`create successfully`,
                    icon: 'success',
                    })
                    console.log(data)
                    history.push('/admin');
                } else {
                    alert ('error')
                }
                })
            }  
            console.log(productName)
            console.log(imageLink)
            console.log(description)
            console.log(categories)
            console.log(price)
        return (
            <Fragment>
                <AdminBanner head={create} />
                    <div className="container createContainer">
                        <Form onSubmit={(event) => createProduct(event)}>
                            <div>
                                <Form.Label>Name:</Form.Label>
                                <Form.Control type="text" value={productName} onChange={event => {setProductName(event.target.value)}} placeholder="Product Name" required />
                            </div>
                            <Form.Label>Image Link:</Form.Label>
                                <Form.Control type="text" value={imageLink} onChange={event => {setImageLink(event.target.value)}} placeholder="Image Link/Url" required />
                            <Form.Label>Description:</Form.Label>
                                <Form.Control as="textarea" rows={3} type="text" value={description} onChange={event => {setDescription(event.target.value)}} placeholder="Product Description" required />
                            <span className="w-50">
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number"  value={price} onChange={event => {setPrice(event.target.value)}} placeholder="Price" required />
                            </span>
                            <Form.Label>Category:</Form.Label>
                            <Form.Control as="select" value={categories} onChange={event => {setCategories(event.currentTarget.value)}}>
                                <option disabled value="" >Select Category Here</option>
                                <option value="Mobile Phone">Mobile Phone</option>
                            </Form.Control>
                            <div className="text-center mb-3">
                                <Button variant="outline-dark" id="createAcctButton" className='btn btn-block' onClick={AddProduct}> Create Product </Button>
                                <Button variant="outline-dark" id="createAcctButton" className='btn btn-block' as={Link} to="/admin"> Go Back to Admin Dashboard </Button>
                            </div>
                        </Form>
                    </div>
            </Fragment>
        )    
}