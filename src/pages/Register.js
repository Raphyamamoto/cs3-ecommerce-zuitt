import {useState,useEffect, Fragment} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import React from 'react';
import { useHistory } from 'react-router-dom';


export default function Register() {
    const history = useHistory();
    const [ firstName, setFirstName ] = useState("");
    const [ middleName, setMiddleName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ password1, setPassword1 ] = useState("");
    const [ password2, setPassword2 ] = useState("");
    const [ mobileNo, setMobileNo ] = useState("");
    const [gender, setGender] = useState("Select Gender Here:");

    //mobile number
    const phStartNumber = ["0995", "0917", "0998", "0908", "0961", "0999", "0919", "1975" ];
    let phoneNumber = mobileNo.toString()
    let everyNumber = phStartNumber.find( element => element === phoneNumber.slice(0,4));
    const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
    const needParam = (firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '' && mobileNo.length === 11);

    
    const [ isComplete, setIsComplete ] = useState(false);
    const [isMatched, setIsMatched] = useState(false);
    const [isValidNo, setIsValidNo] = useState(false);
    
    //register process
    const registerUser = (event) => {
        event.preventDefault(); //this is to avoid page reloading or page redirection

        console.log(typeof firstName);
        console.log(middleName);
        console.log(lastName);
        console.log(email);
        console.log(mobileNo);
        console.log(password1);
        console.log(gender);
        
        fetch(`https://evening-earth-18907.herokuapp.com/user/register/`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({
                firstName: firstName,
                middleName: middleName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNumber: mobileNo,
                gender: gender
            })
        }) 

        .then(res => res.json()) 
        .then(data => {
            console.log(data)
            Swal.fire({
                title: `Hey ${data.firstName}, your account is registered successfully!`,
                icon: 'success',
                text: 'Welcome to Denwa!'
            })

            history.push('/login');
        })
    };

    useEffect(() => {
        if (password1 === password2 && password1 !== "" && password2 !== "") {
            setIsMatched(true);
        }
        else {
            setIsMatched(false); 
        };
    },[password2, password1]);

    useEffect(() => {
         if ((needParam) && (password1 === password2) && mobileNo.slice(0,4) === (everyNumber)) {
            setRegisterBtnActive(true);
            setIsComplete(true);
            
        } else {
            setRegisterBtnActive(false);
            setIsComplete(false); 
            
        }
    },[firstName, middleName, lastName, password1, password2, email, mobileNo, needParam]);

    useEffect(() => {
        const phStartNumber = ["0995", "0917", "0998", "0908", "0961", "0999", "0919", "1975" ];

        let phoneNumber = mobileNo.toString()
        let everyNumber = phStartNumber.find( element => element === phoneNumber.slice(0,4));

        if (mobileNo.slice(0,4) === everyNumber && mobileNo.length === 11){
        setIsValidNo(true)
        }else {
        setIsValidNo(false)
        }
    },[mobileNo]); 

    return(
        <Fragment>
            <Container className="mb-5">
                <div id="createAcct">
                    <h1> Create an Account </h1>
                </div>

                <div id="fillUpformTxt">
                {
                    isComplete ? 
                        <h5 className="mb-5"> Proceed with Register </h5>
                    :   
                        <h5 className="mb-5 text-danger"> Fill up the Form Below </h5>
                }
                </div>

                {/* Register Form */}
                <Form onSubmit={(event) => registerUser(event)} className="mb-5" id="formRegistration">
                    {/* FirstName */}
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name:</Form.Label>
                        <Form.Control type="text" placeholder="Insert First Name Here" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
                    </Form.Group>

                    {/* MiddleName */}
                    <Form.Group controlId="middleName">
                        <Form.Label>Middle Name:</Form.Label>
                        <Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={event => setMiddleName(event.target.value)} required/>
                    </Form.Group>

                    {/* LastName */}
                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name:</Form.Label>
                        <Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={scene => setLastName(scene.target.value)} required/>
                    </Form.Group>

                    {/* Email */}
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address:</Form.Label>
                        <Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={happening => setEmail(happening.target.value)} required/>
                    </Form.Group>

                    {/* Password */}
                    <Form.Group controlId="password1">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password" placeholder="Insert Password Here" value={password1} onChange={event => setPassword1(event.target.value)} required/>
                    </Form.Group>

                    {/* Confirm Password */}
                    <Form.Group controlId="password2">
                        <Form.Label>Confirm Password:</Form.Label>
                        {password1 === "" && password2 === "" ?
                        <strong className="text-success"></strong> :

                        isMatched ?
                            <strong className="text-success"> *Passwords Matched* </strong>
                        :
                            <strong className="text-danger"> *Passwords Should Match* </strong>
                        }
                        <Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={event => setPassword2(event.target.value)} required/>
                    </Form.Group>

                    {/* Gender */}
                    <Form.Group controlId="gender">
                        <Form.Label>Select Gender:</Form.Label>
                        <Form.Control as="select" value={gender} onChange={(click) => setGender(click.currentTarget.value)} required>
                            <option value="" selected disabled>Select Gender Here:</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </Form.Control>
                    </Form.Group>

                    {/* Mobile Number */}
                    <Form.Group>
                        <Form.Label>Mobile Number:</Form.Label>
                        {mobileNo === "" ?
                            <strong className="text-success"></strong>:
                        String(mobileNo).length !== 11 ? 
                            <strong className="text-danger"> *11 digit is Required* </strong>
                        :
                        isValidNo ?  
                            <strong className="text-success"> *Mobile Number is Valid*</strong> 
                        : 
                            <strong className="text-danger"> *Please input a valid number ["0995***", "0917***", "0998***", "0908***", "0961***", "0999***", "0919***", "1975***"]* </strong>
                        }

                        <Form.Control type="number" maxLength={11} placeholder="Insert Mobile Number" value={mobileNo} onInput={(e) => (e.target.value = e.target.value.slice(0, 11))} onChange={pangyayari => setMobileNo(pangyayari.target.value)} required />
                    </Form.Group>

                        {/* register button */}
                    <div id="registerbtn">
                    {isRegisterBtnActive ? 
                        <Button variant="success" className="btn btn-block mb-5" type="submit" id="submitBtn">
                            Create New Account
                        </Button>
                        : 
                        <Button variant="danger" className="btn btn-block mb-5" type="submit" id="submitBtn" disabled>
                            Create New Account
                        </Button>
                    }
                    </div>
                    <div id="spanOr">
                        <div>
                            <p> 
                            <span className="mb-3"> ------OR------ </span>
                            </p>
                        </div>
                        <div>
                            <a href="./Login">
                            <Button id="signInBtn" variant="outline-dark" className='btn btn-block'>
                            Sign In
                            </Button>
                            </a>
                        </div>
                    </div>
                </Form>
            </Container>
        </Fragment>
    )
}