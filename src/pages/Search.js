import React from "react";
import Hero from "../components/Banner";
import searchProducts from "../components/AppNavBar";

export default function searchBar() {
    return(
        <div>
            {searchProducts}
        </div>
    )
}
// import React, { useState } from 'react'
// import {NavLink} from 'react-router-dom';
// import {Form, Button} from 'react-bootstrap';



// function Search( { history }) {

//     const [keyword, setKeyword] = useState('');
//     const searchHandler = (e) => {
//         e.preventDefault()
    
//         if (keyword.trim) {
//             history.pushState(`/search/${keyword}`)
//         } else {
//             history.pushState('/');
//         }
//     }
//     return (
//         <div>
//              <Form onSubmit={searchHandler} className="d-flex">
//                 <Form.Control type="search" placeholder="Search" className="me-2 ml-3" aria-label="Search" onClick={event => setKeyword(event.target.value)} />
//                 <Button as={NavLink} to="/search" variant="outline-light">Search</Button>
//             </Form>
//         </div>
//     )
// }

// export default Search
